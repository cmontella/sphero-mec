extern crate crossbeam_channel;
use mech_core::*;
use mech_utilities::*;
//use std::sync::mpsc::{self, Sender};
use std::thread::{self};
use crossbeam_channel::Sender;
use std::collections::HashMap;
use std::io;
use std::io::prelude::*;

use sphero_rs::sphero::*;
use sphero_rs::driving::*;
use sphero_rs::user_io::*;

lazy_static! {
  static ref SPHERO_ROBOT: u64 = hash_str("sphero/robot");
  static ref HEADING: u64 = hash_str("heading");
  static ref SPEED: u64 = hash_str("speed");
  static ref DISPLAY: u64 = hash_str("display");
}

export_machine!(sphero_robot, sphero_robot_reg);

extern "C" fn sphero_robot_reg(registrar: &mut dyn MachineRegistrar, outgoing: Sender<RunLoopMessage>) -> String {
  registrar.register_machine(Box::new(Robot{outgoing}));
  "#sphero/robot[|heading<f32> speed<f32> display<string>|]".to_string()
}

#[derive(Debug)]
pub struct Robot {
  outgoing: Sender<RunLoopMessage>,
  //printed: usize,
}

impl Machine for Robot {

  fn name(&self) -> String {
    "sphero/robot".to_string()
  }

  fn id(&self) -> u64 {
    hash_str(&self.name())
  }

  fn on_change(&mut self, table: &Table) -> Result<(), MechError> {
    let mut sphero = Sphero::new("SB-C714").unwrap();
    sphero.power.wake();

    let heading = table.get(&TableIndex::Index(1),&TableIndex::Alias(*HEADING))?;
    let speed = table.get(&TableIndex::Index(1),&TableIndex::Alias(*SPEED))?;
    let display = table.get(&TableIndex::Index(1),&TableIndex::Alias(*DISPLAY))?;


    match display {
      Value::String(string) => {
        sphero.user_io.set_led_matrix_picture(string.to_string());
      }
      _ => (),
    }



    Ok(())
  }
}