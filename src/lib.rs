extern crate mech_core;
#[macro_use]
extern crate mech_utilities;
extern crate crossbeam_channel;
#[macro_use]
extern crate lazy_static;
extern crate sphero_rs;
//use mech_core::{Interner, Transaction};
//use mech_core::Value;

pub mod robot;